import React from "react";
import styled from "styled-components";
import { StaticImage } from "gatsby-plugin-image";

// CSS-in-JS styles
const FooterContainer = styled.footer`
  font-family: "Montserrat";
  font-size: 0.875rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

// Footer markup
function Footer() {
  return (
    <FooterContainer>
      {/* Displays the copywrite symbol and current year */}
      <p>&#169;{new Date().getFullYear()} Nathan Pope</p>
      <a href="https://www.gatsbyjs.com/">
        <StaticImage
          src="../images/Gatsby-Logo.svg"
          alt="Made with Gatsby"
          placeholder="blurred"
          layout="fixed"
        />
      </a>
    </FooterContainer>
  );
}

export default Footer;
