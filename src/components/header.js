import React from "react";
import styled from "styled-components";
import AniLink from "gatsby-plugin-transition-link/AniLink";

// CSS-in-JS Styles
// Brand styles
const Brand = styled(AniLink)`
  font-family: "Roboto Slab";
  font-size: 1.33rem;
  color: white;
  background-color: black;
  padding: 0.25rem 0.625rem;
`;

// Navbar styles
const Navbar = styled.nav`
  // Layout
  display: flex;
  justify-content: space-between; // Space links on opposite sides
  align-items: center; // Vertically aligns links
`;

// Markup
// Brand link markup
function BrandLink() {
  return (
    <Brand cover direction="right" bg="#000000" to="/">
      Nathan Pope
    </Brand>
  );
}

// Header markup
function Header() {
  return (
    <header>
      <Navbar>
        {/* Brand link */}
        <BrandLink />
        {/* Projects link */}
        <AniLink
          className="primary-link"
          activeClassName="secondary-link-black"
          cover
          direction="left"
          bg="#000000"
          to="/portfolio"
        >
          Projects
        </AniLink>
      </Navbar>
    </header>
  );
}

export default Header;
