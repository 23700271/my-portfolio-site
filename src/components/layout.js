import React from "react";
// Components
import Header from "../components/header";
import Footer from "../components/footer";
// Packages
import styled, { createGlobalStyle } from "styled-components";
import { Helmet } from "react-helmet";
// Fonts
import "@fontsource/roboto-slab/700.css";
import "@fontsource/montserrat/700.css";
import "@fontsource/open-sans";

// CSS-in-JS
// Global styles
const GlobalStyle = createGlobalStyle`
  body {
    font-family: "Open Sans";
    h1,
    h2,
    h3 {
      font-family: "Montserrat"; // Font family for all headings
      margin: 0; // Remove default margins
    }
    h1 {
      font-size: 3.25rem; // Main heading size
      text-align: center;
    }
    h2 {
      font-size: 1.75rem; // Secondary heading size
    }
    h3 {
      font-size: 1.25rem; // Third heading size
      margin-top: 2rem;
    }
    a {
      font-family: "Montserrat";
      text-decoration: none;
    }
    li {
      margin: 0.5rem 0;
    }
  }
  article {
    margin: 6rem auto; // Vertical margin and horizontal auto for centering content
    padding: 1.5rem; // Padding around article content
    color: #FFFFFF; // White font colour
    background-color: #222222; // Dark grey background
    box-shadow: 1rem 1rem 0rem #000000; // Black shadow with no blur
  }  
  /* Large screen devices */
  @media screen and (min-width: 50em) {
    html {
      font-size: 1.125em;
    }
    body {
      h1 {
        font-size: 5.33rem; // Main heading size
      }
      h2 {
        font-size: 2rem; // Secondary heading size
      }
      h3 {
        font-size: 1.33rem; // Third heading size
      }
    }
  }
  /* Link hover animation transition time */
  a {
    transition: 0.25s;
  }
  /* Hero banner shared styles class */
  .hero {
    text-align: center; // Center aligns content
    margin: 8rem auto;
  }
  /* CTA Green primary links */
  .primary-link {
    padding: 0.25rem 0.625rem;
    color: white;
    border: 0.125rem solid #00691e; // Green border
    background-color: #00691e; // Green background
    :hover {
      border: 0.125rem solid #b60000; // Red border
      background-color: #b60000; // Red background
    }
  }
  /* Secondary links with a white border and font */
  .secondary-link-white {
    padding: 0.25rem 0.625rem;
    color: white;
    border: 0.125rem solid white; // White border
    :hover {
      border: 0.125rem solid #b60000; // Red border
    }
  }
  /* Secondary links with a black border and font */
  .secondary-link-black {
    padding: 0.25rem 0.625rem;
    color: black;
    background-color: white;
    border: 0.125rem solid black; // White border
    :hover {
      background-color: white;
      border: 0.125rem solid #b60000; // Red border
    }
  }
  /* Standard content container styles */
  .content-container {
    max-width: 40rem;
  }
  /* Project page class styles */
  /* Project pages hero banner */
  .project-hero {
    h3 {
      margin-bottom: 2rem;
    }
  }
  /* Project pages hero links container for repository and visit site links */
  .project-hero-link-container {
    display: flex; // Use flexbox
    justify-content: center; // Horizontally center links
    gap: 1rem; // Gap between links
    a {
      padding: 0.4rem 0.8rem;
    }
  }
  /* Image container for project pages */
  .project-image-container {
    text-align: center;
    padding: 0.5rem 0;
  }
  /* Image group container for multiple images on project pages */
  .project-image-group-container {
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 0.5rem;
    @media screen and (min-width: 40em) {
      flex-direction: row;
      gap: 1rem;
    }
  }
`;

// Container styles
const Container = styled.div`
  margin: 0 auto; // No margin top and bottom & center horizontally
  max-width: 67rem; // Limits maximum page width
  padding: 1rem; // Outer page padding
`;

// Markup
function Layout({ children }) {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <html lang="en-GB" />
        <meta name="description" content="Nathan Pope's portfolio site." />
      </Helmet>
      <GlobalStyle />
      <Container>
        <Header />
        <main>{children}</main>
        <Footer />
      </Container>
    </>
  );
}

export default Layout;
