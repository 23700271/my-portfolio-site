import React from "react";
import Layout from "../../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { Helmet } from "react-helmet";

// Portfolio site page markup
function PortfolioSitePage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Portfolio Site Project</title>
      </Helmet>
      {/* Hero banner section */}
      <section className="hero project-hero">
        <h1>Portfolio Site</h1>
        <h3>UX Design &#38; Front-end Development</h3>
        {/* Hero links container */}
        <div className="project-hero-link-container">
          {/* Repository link */}
          <a
            className="primary-link"
            href="https://bitbucket.org/23700271/my-portfolio-site/src/master/"
          >
            Repository
          </a>
          {/* Website link */}
          <a
            className="secondary-link-black"
            href="https://nathanpope.netlify.app/"
          >
            Visit site
          </a>
        </div>
      </section>
      <article className="content-container">
        <h2>Design and development process</h2>
        <p>
          I recently remade this portfolio site from scratch using the Gatsby
          framework. Not only is this my first project with Gatsby, but it's
          also my first time working with styled components.
        </p>
        <p>
          Starting with pen and paper, I noted down my requirements and sketched
          out some low-fidelity drawings. I also looked at existing portfolio
          sites for inspiration.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/portfolio-site/portfolio-site-planning.jpg"
              alt="Portfolio site early sketches and planning."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/portfolio-site/portfolio-site-figma.png"
              alt="Portfolio site Figma designs."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          Next, I created high-fidelity prototypes of the portfolio site using
          Figma to experiment more with the styles and layout in detail. Once I
          was happy with the design, I set up a Gatsby project and developed the
          planned functionality. Finally, I styled the website using my
          high-fidelity prototypes for reference.
        </p>
        <div className="project-image-container">
          <StaticImage
            src="../../images/portfolio-site/portfolio-site-home.png"
            alt="Portfolio site home page."
            placeholder="blurred"
            layout="constrained"
            height={500}
          />
        </div>
        <p>
          Feel free to take a look at the code yourself by following the
          repository link at the top of the page.
        </p>
      </article>
    </Layout>
  );
}

export default PortfolioSitePage;
