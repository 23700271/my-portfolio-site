import React from "react";
import Layout from "../../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { Helmet } from "react-helmet";

// Bio-store page markup
function BioStorePage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Bio-Store Project</title>
      </Helmet>
      {/* Hero banner section */}
      <section className="hero project-hero">
        <h1>Bio-Store</h1>
        <h3>UX Design &#38; Front-end Development</h3>
        {/* Hero links container */}
        <div className="project-hero-link-container">
          {/* Repository link */}
          <a
            className="primary-link"
            href="https://bitbucket.org/23700271/bio-store/src/master/"
          >
            Repository
          </a>
          {/* Website link */}
          <a
            className="secondary-link-black"
            href="https://bio-store-23700271.netlify.app/"
          >
            Visit site
          </a>
        </div>
      </section>
      {/* Main content */}
      <article className="content-container">
        <h2>The design and development process</h2>
        <p>
          At university I created two websites for real clients. The Bio-Store
          was one of these sites based on a fictional company. The company was
          based in North West England and sold biodegradable products to
          businesses around the world.
        </p>
        <div className="project-image-container">
          {/* Bio-store website homepage screenshot */}
          <StaticImage
            src="../../images/bio-store/bio-store-home.webp"
            alt="Bio-Store website home page."
            placeholder="blurred"
            layout="constrained"
            height={500}
          />
        </div>
        <p>
          This project was a full design and development build starting with a
          website design brief provided by the company. Initial steps focused on
          identifying requirements and planning the layout.
        </p>
        <div className="project-image-group-container">
          {/* Bio-store mobile figma designs screenshot */}
          <div className="project-image-container">
            <StaticImage
              src="../../images/bio-store/bio-store-figma-mobile.webp"
              alt="Figma high-fidelity designs of Bio-Store website for mobile layout."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          {/* Bio-store desktop figma designs screenshot */}
          <div className="project-image-container">
            <StaticImage
              src="../../images/bio-store/bio-store-figma-desktop.webp"
              alt="Figma high-fidelity designs of Bio-Store website for desktop layout."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          Continuing from the initial design phase, high-fidelity prototypes
          were designed for mobile and desktop devices using Figma. Once the
          website was fully planned out it was developed with React, starting
          with functionality and then design.
        </p>
        <div className="project-image-group-container">
          {/* Bio-store mobile figma designs screenshot */}
          <div className="project-image-container">
            <StaticImage
              src="../../images/bio-store/bio-store-team.webp"
              alt="Bio-Store website team page."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          {/* Bio-store desktop figma designs screenshot */}
          <div className="project-image-container">
            <StaticImage
              src="../../images/bio-store/bio-store-products.webp"
              alt="Bio-Store website products page."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
      </article>
    </Layout>
  );
}

export default BioStorePage;
