import React from "react";
import Layout from "../../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { Helmet } from "react-helmet";

// The Bee Centre page markup
function TheBeeCentrePage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | The Bee Centre Project</title>
      </Helmet>
      {/* Hero banner section */}
      <section className="hero project-hero">
        <h1>The Bee Centre</h1>
        <h3>UX Design &#38; Front-end Development</h3>
        {/* Hero links container */}
        <div className="project-hero-link-container">
          {/* Repository link */}
          <a
            className="primary-link"
            href="https://bitbucket.org/23700271/the-bee-centre/src/master/"
          >
            Repository
          </a>
          {/* Website link */}
          <a
            className="secondary-link-black"
            href="https://the-bee-centre-23700271.netlify.app/"
          >
            Visit site
          </a>
        </div>
      </section>
      {/* Main content */}
      <article className="content-container">
        <h2>The design and development process</h2>
        <p>
          The Bee Centre project was developed at university for a fictional
          company based in North West England. The client wanted a website to
          promote their business and encourage people to get in touch.
        </p>
        <div className="project-image-container">
          <StaticImage
            src="../../images/the-bee-centre/bee-centre-home.webp"
            alt="The Bee Centre website home page."
            placeholder="blurred"
            layout="constrained"
            height={500}
          />
        </div>
        <p>
          Using a website design brief provided by the company, the requirements
          for the website were identified. Research was undertaken on similar
          websites to understand the target audience and market. Once the
          details were worked out, low-fidelity prototypes were sketched to plan
          the website layout.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/the-bee-centre/spider-diagram.webp"
              alt="Spider diagram planning for The Bee Centre website."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/the-bee-centre/sketches.webp"
              alt="Early paper sketches and planning for The Bee Centre website."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          Following on from the low-fidelity sketches, high-fidelity prototypes
          were then created using Figma to design the website in much greater
          detail. With the Figma designs for reference the website was then
          developed using HTML5 and Sass. Additional changes were made based on
          the clients feedback throughout the process.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/the-bee-centre/bee-centre-figma.webp"
              alt="Figma designs for The Bee Centre website."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/the-bee-centre/bee-centre-team.webp"
              alt="The Bee Centre website team page."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
      </article>
    </Layout>
  );
}

export default TheBeeCentrePage;
