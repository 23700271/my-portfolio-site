import React from "react";
import Layout from "../../components/layout";
import { StaticImage } from "gatsby-plugin-image";
import { Helmet } from "react-helmet";

// Lets Get Quacking page markup
function LetsGetQuackingPage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Let's Get Quacking! Project</title>
      </Helmet>
      {/* Hero banner section */}
      <section className="hero project-hero">
        <h1>Let's Get Quacking!</h1>
        <h3>UX Design, Front-end Development &#38; Back-end Design</h3>
        {/* Hero links container */}
        <div className="project-hero-link-container">
          {/* Repository link */}
          <a
            className="primary-link"
            href="https://bitbucket.org/23700271/recipe-app/src/master/"
          >
            Repository
          </a>
          {/* Website link */}
          <a
            className="secondary-link-black"
            href="https://lets-get-quacking-23700271.netlify.app/"
          >
            Visit site
          </a>
        </div>
      </section>
      {/* Main content */}
      <article className="content-container">
        <h2>Research and development process</h2>
        <p>
          For my research and development project at university, I made a recipe
          app for students. The app would also work offline using progressive
          web app features. Varied types of research were carried out to
          understand student eating habits, including background and literature
          review and related works. I also analysed existing recipe apps and
          conducted a user study on student eating habits to establish
          requirements.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/use_case_diagram.png"
              alt="Let's Get Quacking use case UML diagram."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/entity_relationship_diagram.png"
              alt="Let's Get Quacking entity-relationship diagram."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          The modelling process included user stories, use cases, sequence
          diagrams, a class diagram, and an entity-relationship diagram. For the
          design, I made low-fidelity sketches of the interface to get an idea
          of how the content would be structured in practice. The app name,
          colour scheme, and logo were also considered in detail. Finally, I
          selected the technologies to develop, test and deploy the website.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/home_sketch.jpg"
              alt="Let's Get Quacking home page sketches."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/logo_ideas.jpg"
              alt="Sketches of logo ideas for Let's Get Quacking."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          The use cases I wrote earlier were used to create behaviour-driven
          development (BDD) tests with CodeceptJS to implement the apps main
          features. I used Kanban agile software development to manage the
          project too. The app was made with React, I used a Git repository for
          version control, and the final build was deployed to Netlify.
        </p>
        <div className="project-image-group-container">
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/lets-get-quacking-home.png"
              alt="Let's Get Quacking web app home page."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
          <div className="project-image-container">
            <StaticImage
              src="../../images/lets-get-quacking/lets-get-quacking-recipes.png"
              alt="Let's Get Quacking web app recipes page."
              placeholder="blurred"
              layout="constrained"
              height={500}
            />
          </div>
        </div>
        <p>
          Once the recipe app had been completed I ran another user study to
          determine whether the project was successful. The Covid-19 pandemic
          disrupted my studies which limited the scope of the user study, but
          ultimately the project was successful based on my findings.
        </p>
      </article>
    </Layout>
  );
}

export default LetsGetQuackingPage;
