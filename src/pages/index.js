import React from "react";
import Layout from "../components/layout";
import styled from "styled-components";
import { Helmet } from "react-helmet";
import AniLink from "gatsby-plugin-transition-link/AniLink";

// CSS-in-JS styles
// Hero banner styles (Main heading & My Projects CTA link)
const Hero = styled.section`
  // Main heading styles
  h1 {
    margin-bottom: 3.5rem; // Margin between heading and link
  }
  // Hero link styles
  a {
    font-size: 1.25rem;
    padding: 0.4rem 2rem;
  }
`;

// About me markup
function About() {
  return (
    <article className="content-container">
      <h2>Hey, I'm Nathan.</h2>
      <p>
        I’m a first-class graduate in Web Design and Development from Edge Hill
        University and winner of two academic achievement awards. I have
        experience in UX design, front-end development, and back-end
        development.
      </p>
      <p>
        For my final year at university I completed a research and development
        project to create a student recipe progressive web app. I also designed
        and developed three portfolio websites.
      </p>
      <p>
        I’m currently looking for work as a front-end developer and UX designer
        but, I am open to offers in other roles. If you’d like to get in touch
        then you can email me at <b>nathanwpope@gmail.com</b>.
      </p>
    </article>
  );
}

// My skills markup
function Skills() {
  return (
    <article className="content-container">
      <h2>My Skills</h2>
      {/* UX / UI Design */}
      <section>
        <h3>UX / UI Design</h3>
        <p>
          Creating websites that are accessible and easy to use for all through
          careful planning, design, research and testing. Techniques used
          include, personas, low- and high-fidelity prototypes, user studies,
          data analysis and more.
        </p>
      </section>
      {/* Front-end Development */}
      <section>
        <h3>Front-end Development</h3>
        <p>
          Developing well-structured, clean and efficient client-side code for
          websites and web apps using a range of technologies including:
        </p>
        <ul>
          <li>JavaScript</li>
          <li>React</li>
          <li>Ember.js</li>
          <li>Node.js</li>
          <li>HTML5 &#38; CSS3</li>
          <li>Sass</li>
          <li>Bootstrap</li>
        </ul>
      </section>
      {/* Back-end Development */}
      <section>
        <h3>Back-end Development</h3>
        <p>
          Developing effective back-end software and databases for web apps with
          the following technologies:
        </p>
        <ul>
          <li>PHP</li>
          <li>Laravel including Eloquent ORM with MySQL</li>
          <li>Composer</li>
        </ul>
      </section>
      {/* Other */}
      <section>
        <h3>Other</h3>
        <p>Additional skills with technologies, tools and operating systems:</p>
        <ul>
          <li>Python</li>
          <li>Java</li>
          <li>Software version control - Git &#38; Mercurial</li>
          <li>Command-line interface (CLI)</li>
          <li>Linux - Ubuntu / Kubuntu</li>
          <li>Microsoft Windows</li>
        </ul>
      </section>
    </article>
  );
}

// Home page markup
function HomePage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Web Designer &#38; Developer</title>
      </Helmet>
      {/* Hero section */}
      <Hero className="hero">
        {/* Main heading */}
        <h1>
          Web Designer &#38;<br></br>Developer
        </h1>
        {/* My Projects call to action link */}
        <AniLink
          className="primary-link"
          cover
          direction="left"
          bg="#000000"
          to="/portfolio"
        >
          My Projects
        </AniLink>
      </Hero>
      {/* About section */}
      <About />
      {/* My Skills section */}
      <Skills />
    </Layout>
  );
}

export default HomePage;
