import React from "react";
import Layout from "../components/layout";
import styled from "styled-components";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import { Helmet } from "react-helmet";

// Styles
const Container = styled.section`
  text-align: center;
  margin: 9rem auto;
  h1,
  h2 {
    padding-bottom: 4rem;
  }
  a {
    font-size: 1.25rem;
    padding: 0.4rem 2rem;
  }
`;

// 404 page not found markup
function NotFoundPage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Page Not Found</title>
      </Helmet>
      <Container>
        <h1>Page Not Found</h1>
        <h2>Sorry but this page doesn't exist.</h2>
        <AniLink
          className="primary-link"
          cover
          direction="right"
          bg="#000000"
          to="/"
        >
          Go home
        </AniLink>
      </Container>
    </Layout>
  );
}

export default NotFoundPage;
