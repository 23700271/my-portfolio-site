import React from "react";
import Layout from "../components/layout";
import styled from "styled-components";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import { StaticImage } from "gatsby-plugin-image";
import { Helmet } from "react-helmet";

// Project article container styles
const ProjectContainer = styled.article`
  /* Layout */
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  gap: 1rem;
  /* General */
  max-width: 25rem;
  ul {
    font-family: "Montserrat";
    list-style: none;
    padding: 0;
  }
  section {
    max-width: 25rem;
    display: flex;
    flex-direction: column;
    align-items: space-between;
  }
  @media screen and (min-width: 66em) {
    max-width: 55rem;
    flex-direction: row;
  }
`;

// Project link container styles
const LinkContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 1rem;
  margin-top: 1rem;
`;

// Image container styles
const ImageContainer = styled.div`
  display: flex; // Use flexbox
  justify-content: center; // Horizontally center aligns
  align-items: center; // Vertically center aligns
  max-width: 25rem; // Limit image max width
`;

// Project image selector (returns an image depending on the project title)
// Gatsby StaticImage does not support component props yet so a selector is needed.
function ProjectImage(props) {
  const PROJECT_TITLE = props.projectTitle; // Project title prop

  // Checks whether the project title matches one of the available images and returns it.
  if (PROJECT_TITLE === "The Bee Centre") {
    return (
      <StaticImage
        src="../images/the-bee-centre/bee-centre-home.webp"
        alt="The Bee Centre website home page."
        placeholder="blurred"
        height={300}
      />
    );
  } else if (PROJECT_TITLE === "Bio-Store") {
    return (
      <StaticImage
        src="../images/bio-store/bio-store-home.webp"
        alt="Biodegradable Store website home page."
        placeholder="blurred"
        layout="constrained"
        height={300}
      />
    );
  } else if (PROJECT_TITLE === "Portfolio Site") {
    return (
      <StaticImage
        src="../images/portfolio-site/portfolio-site-home.png"
        alt="Portfolio site home page."
        placeholder="blurred"
        layout="constrained"
        height={300}
      />
    );
  } else if (PROJECT_TITLE === "Let's Get Quacking!") {
    return (
      <StaticImage
        src="../images/lets-get-quacking/lets-get-quacking-home.png"
        alt="Let's Get Quacking home page."
        placeholder="blurred"
        layout="constrained"
        height={300}
      />
    );
  } else {
    return (
      <StaticImage
        src="../images/icon.png"
        alt="Placeholder Gatsby icon."
        placeholder="blurred"
        layout="constrained"
        height={300}
      />
    );
  }
}

// Project section markup
function Project(props) {
  // Maps project skills to a list with list item markup
  const LIST_SKILLS = props.skills.map((skill) => <li>{skill}</li>);

  return (
    <ProjectContainer>
      {/* Image */}
      <ImageContainer>
        <ProjectImage projectTitle={props.title} />
      </ImageContainer>
      <section>
        <div>
          {/* Title */}
          <h2>{props.title}</h2>
          {/* Skills list */}
          <ul>{LIST_SKILLS}</ul>
          {/* Description */}
          <p>{props.description}</p>
        </div>
        {/* Learn more and site link container */}
        <LinkContainer>
          {/* Learn more link */}
          <AniLink
            cover
            direction="down"
            bg="#000000"
            className="primary-link"
            to={"/portfolio/" + props.page}
          >
            Learn more
          </AniLink>
          {/* Visit site link */}
          <a className="secondary-link-white" href={props.url}>
            Visit site
          </a>
        </LinkContainer>
      </section>
    </ProjectContainer>
  );
}

// Portfolio page markup
function PortfolioPage() {
  return (
    <Layout>
      <Helmet>
        <title>Nathan Pope | Portfolio Projects</title>
      </Helmet>
      {/* Hero banner with main heading */}
      <section className="hero">
        <h1>
          Portfolio<br></br>Projects
        </h1>
      </section>
      {/* Portfolio Site project */}
      <Project
        title="Portfolio Site"
        skills={["UX / UI Design", "Front-end Development"]}
        description="A little portfolio website made with Gatsby to showcase my projects and tell you a bit about me."
        page="portfolio-site"
        url="https://nathanpope.netlify.app/"
      />
      {/* Let's Get Quacking! project */}
      <Project
        title="Let's Get Quacking!"
        skills={["UX / UI Design", "Front-end Development", "Back-end Design"]}
        description="A recipe app for students with offline functionality. Researched, designed and developed for my final year project at university. Made with React."
        page="lets-get-quacking"
        url="https://lets-get-quacking-23700271.netlify.app/"
      />
      {/* Bio-Store project */}
      <Project
        title="Bio-Store"
        skills={["UX / UI Design", "Front-end Development"]}
        description="A website designed and developed at university for the Bio-Store, a fictional company with a real client and design brief. Made with React."
        page="bio-store"
        url="https://bio-store-23700271.netlify.app/"
      />
      {/* The Bee Centre project */}
      <Project
        title="The Bee Centre"
        skills={["UX / UI Design", "Front-end Development"]}
        description="Designed and developed at university, this fun website was made for The Bee Centre, a fictional company with a real client and design brief. Made with HTML5 and Sass."
        page="the-bee-centre"
        url="https://the-bee-centre-23700271.netlify.app/"
      />
    </Layout>
  );
}

export default PortfolioPage;
